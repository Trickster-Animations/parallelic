import toml
import os


def get_safe_tasks(taskdef:str, finished_tasks:list=[]):
    tasksdef = toml.load(taskdef)
    safe = []
    for task in tasksdef["tasks"]:
        for name, tdef in task.items():
            if ("requires" not in tdef or set(finished_tasks).issuperset(set(tdef["requires"])))\
                and name not in finished_tasks:
                safe.append(name)
    return safe


def get_task(taskdef:str, task_name:str):
    tasksdef = toml.load(taskdef)
    for task in tasksdef["tasks"]:
        if task_name in task:
            return task[task_name]


def get_task_type(taskdef:str, task_name:str):
    return get_task(taskdef, task_name)["type"]


def get_setup_check(taskdef: str, task_name:str):
    return (
        get_task(taskdef, task_name)["check"]["script"]
    )


def get_setup_script(taskdef:str, task_name:str):
    return (
        get_task(taskdef, task_name)["setup"]
    )


def get_task_script(taskdef:str, task_name:str):
    return get_task(taskdef, task_name)["script"]