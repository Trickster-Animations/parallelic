from parallelic.util import parser


def test_all_safe():
    assert set(parser.get_safe_tasks("tests/resources/allsafe.toml")) == set(["task1", "task2", "task3"])


def test_dependents():
    assert set(parser.get_safe_tasks("tests/resources/requires.toml")) == set(["safe1", "safe2"])
    assert set(parser.get_safe_tasks("tests/resources/requires.toml", ["safe1"])) == set(["safe2", "rsafe1"])
    assert set(parser.get_safe_tasks("tests/resources/requires.toml", ["safe2"])) == set(["safe1", "rsafe2"])
    assert set(parser.get_safe_tasks("tests/resources/requires.toml", ["safe1", "safe2"])) == set(["rsafe1", "rsafe2", "rsafe12"])


def test_tasktypes():
    assert parser.get_task_type("tests/resources/allsafe.toml", "task1") == "sequential"
    assert parser.get_task_type("tests/resources/allsafe.toml", "task2") == "distributed"
    assert parser.get_task_type("tests/resources/allsafe.toml", "task3") == "sequential"